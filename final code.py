dr = [1, 0, 0, -1]
dc = [0, 1, -1, 0]
def solve(r, c, count, m, n, tc, ch1, ch2, ch3, pt, flag, counter):
    if count == ch1 and (r != pt[0][0] and c != pt[0][1]):
        return
    if count == ch2 and (r != pt[1][0] and c != pt[1][1]):
        return
    if count == ch3 and (r != pt[2][0] and c != pt[2][1]):
        return
    if r == pt[0][0] and c == pt[0][1] and count != ch1:
        return
    if r == pt[1][0] and c == pt[1][1] and count != ch2:
        return
    if r == pt[2][0] and c == pt[2][1] and count != ch3:
        return
    if count == tc:
        if r == 0 and c == 1:
            counter[0] += 1
        return
    for i in range(4):
        x, y = r + dr[i], c + dc[i]
        if x < m and y < n and x >= 0 and y >= 0 and not flag[x][y]:
            flag[x][y] = True
            solve(x, y, count + 1, m, n, tc, ch1, ch2, ch3, pt, flag, counter)
            flag[x][y] = False
case_num = 1
while True:
        m, n = map(int,input().split())
        if m + n == 0:
            break
        check_in = input().split()
        pt = [(int(check_in[i]), int(check_in[i+1])) for i in range(0, len(check_in), 2)]
        flag = [[False for _ in range(n)] for _ in range(m)]
        flag[0][0] = True
        counter = [0]
        tc = m * n
        ch1 = int(tc / 4)
        ch2 = int(tc / 2)
        ch3 = int(3 * tc /4)
        solve(0, 0, 1, m, n, tc, ch1, ch2, ch3, pt, flag, counter)
        print(f"Case {case_num}: {counter[0]}")
        case_num += 1

