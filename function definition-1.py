dr = [1, 0, 0, -1]
dc = [0, 1, -1, 0]
def solve(r, c, count, m, n, tc, ch1, ch2, ch3, pt, flag, counter):
    if count == ch1 and (r != pt[0][0] and c != pt[0][1]):
        return
    if count == ch2 and (r != pt[1][0] and c != pt[1][1]):
        return
    if count == ch3 and (r != pt[2][0] and c != pt[2][1]):
        return
    if r == pt[0][0] and c == pt[0][1] and count != ch1:
        return
    if r == pt[1][0] and c == pt[1][1] and count != ch2:
        return
    if r == pt[2][0] and c == pt[2][1] and count != ch3:
        return
    if count == tc:
        if r == 0 and c == 1:
            counter[0] += 1
        return
